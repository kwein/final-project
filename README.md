# My project's README

#Lab Notebook
##PracticalComputing2017

###Transdecoder already ran
###all databases compiled

###BLAST
#uniprot_sprot.pep path in /media/RAID/sequence_databases/
  	 blastx -query trinity_out.Trinity.fasta -db /media/RAID/sequence_databases/uniprot_sprot.pep -num_threads 6 -max_target_seqs 1 -outfmt 6 > blastx.outfmt6
   	blastp -query trinity_out.Trinity.fasta.transdecoder.pep -db /media/RAID/sequence_databases/uniprot_sprot.pep -num_threads 6 -max_target_seqs 1 -outfmt 6 > blastp.outfmt6
	#good

###HMMER
	hmmscan --cpu 6 --domtblout TrinotatePFAM.out /media/RAID/sequence_databases/Pfam-A.hmm trinity_out.Trinity.fasta.transdecoder.pep > pfam.log
	    #good (10 hours???)

		###SignalP
			signalp -f short -n signalp.out trinity_out.Trinity.fasta.transdecoder.pep
		#ran, but no results 
		#re-ran - DO NOT USE

###tmhmm
	tmhmm --short < trinity_out.Trinity.fasta.transdecoder.pep > tmhmm.out
		#good

###Rnammer
	~/transcriptome_programs/Trinotate.3.1.1/util/rnammer_support/RnammerTranscriptome.pl --transcriptome trinity_out.Trinity.fasta --path_to_rnammer ~/transcriptome_programs/RNAmmer/rnammer
		#error
		can't locate XML/Simple.pm in @INC you may need to install the XML::Simple module) (INC contains: /home/brewerlab/miniconda3/lib/perl5/site_perl/5.22.0/x86_64-linux-thread-multi /home/brewerlab/miniconda3/lib/perl5/site_perl/5.22.0
		#changed perl scripts, XML file

###Loading databases	
#map file load	
   	~/transcriptome_programs/Trinotate-3.1.1/util/trinityrnaseq-Trinity-v2.3.2/util/support_scripts/get_Trinity_gene_to_trans_map.pl trinity_out.Trinity.fasta > Trinity.fasta.gene_trans_map
	#good

#Loading database(FAIL)
	~/transcriptome_programs/Trinotate-3.1.1/Trinotate /media/RAID/sequence_databases/Trinotate.sqlite init --gene_trans_map Trinity.fasta.gene_trans_map --transcript_fasta trinity_out.Trinity.fasta --transdecoder_pep trinity_out.Trinity.fasta.transdecoder.pep

	~/brewerlab/miniconda3/bin/perl
		#Failed, updated packages

###new location /usr/bin/perl 
###new run
   	~/transcriptome_programs/Trinotate.3.1.1/util/rnammer_support/RnammerTranscriptome.pl --transcriptome trinity_out.Trinity.fasta --path_to_rnammer ~/transcriptome_programs/RNAmmer/rnammer

###Loading databases (done in lab)
   	~/transcriptome_programs/Trinotate-3.1.1/Trinotate Trinotate.sqlite init --gene_trans_map Trinity.fasta.gene_trans_map --transcript_fasta trinity_out.Trinity.fasta --transdecoder_pep trinity_out.Trinity.fasta.transdecoder.pep

##BLAST load
  	 Trinotate Trinotate.sqlite LOAD_swissprot_blastp blastp.outfmt6
  	 Trinotate Trinotate.sqlite LOAD_swissprot_blastx blastx.outfmt6
##Pfam
  	 Trinotate Trinotate.sqlite LOAD_pfam TrinotatePFAM.out

##tmhmm
   		Trinotate Trinotate.sqlite LOAD_tmhmm tmhmm.out
   
##skip signalp

###Annotation report
   		Trinotate Trinotate.sqlite report -E 1e-5 > trinotate_annotation_report.xls